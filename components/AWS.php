<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 11.06.2018
 * Time: 22:35
 */

namespace app\components;


use app\modules\patient\models\LifeAwsSources;
use app\modules\patient\models\Log;
use app\modules\patient\models\Log\S3ErrorLog;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;

class AWS
{
    const DEV_BUCKET = 'dev-savelifeid';
    const STAGE_BUCKET = 'stage-savelifeid';
    const PROD_BUCKET = 'prod-savelifeid';

    /**
     * @param $bucket string
     * @return S3Client
     */
    private function getS3Client($bucket)
    {
        $amazonConfig = [
            'bucket'    => $bucket,
            'region'    => getenv('BUCKET_IMAGE_REGION'),
            'version'    => 'latest',
            'credentials' => [
                'key'    => getenv('BUCKET_ACCESS_KEY'),
                'secret'    => getenv('BUCKET_SECRET_KEY')
            ]
        ];

        return new S3Client($amazonConfig);
    }

    /**
     * @param $bucket LifeAwsSources
     * @param $filename
     * @return bool|string
     */
    public function getContent($bucket, $filename)
    {
        $fileKey = ($bucket->root == '/' ? '' : $bucket->root . '/') . $filename;
        try {
            $s3client = $this->getS3Client($bucket->name);
            $doesExist = $s3client->doesObjectExist($bucket->name, $fileKey);

            if ($doesExist){
                $result = $s3client->getObject([
                    'Bucket' => $bucket->name,
                    'Key'    => $fileKey,
                ]);
                return  (string)$result['Body'];
            }
        } catch (S3Exception $e){
            (new S3ErrorLog([
                'error' => $e->getAwsErrorMessage(),
                'type' => Log::TYPE_S3_GET_CONTENT
            ]))->save();
        }

        return false;
    }

    /**
     * @param $bucket LifeAwsSources
     * @param $filename string
     * @param $content mixed
     * @return bool
     */
    public function setContent($bucket, $filename, $content)
    {
        try {
            $s3client = $this->getS3Client($bucket->name);

            $result = $s3client->putObject([
                'Bucket' => $bucket->name,
                'Key' => $filename,
                'Body' => $content,
                'ACL' => 'private',
            ]);

            $sFileName = $result->get('ObjectURL');
            if ($sFileName) {
                return true;
            }
        } catch (S3Exception $e) {
            (new S3ErrorLog([
                'error' => $e->getAwsErrorMessage(),
                'type' => Log::TYPE_S3_SET_CONTENT
            ]))->save();
        }

        return false;
    }
}