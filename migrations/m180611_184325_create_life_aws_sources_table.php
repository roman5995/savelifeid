<?php

use yii\db\Migration;

/**
 * Handles the creation of table `life_aws_sources`.
 */
class m180611_184325_create_life_aws_sources_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8';
        }

        $this->createTable('life_aws_sources', [
            'name' => $this->string(64)->notNull(),
            'root' => $this->string(64)->notNull()->defaultValue('/'),
            'region' => $this->string(64)->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('pk_life_aws_sources_name', 'life_aws_sources', 'name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('life_aws_sources');
    }
}
