<?php

use app\components\AWS;
use yii\db\Migration;

/**
 * Class m180611_193140_insert_data_to_life_aws_sources_table
 */
class m180611_193140_insert_data_to_life_aws_sources_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('life_aws_sources' , [
            'name' => AWS::DEV_BUCKET,
            'region' => 'us-east-2',
        ]);
        $this->insert('life_aws_sources' , [
            'name' => AWS::STAGE_BUCKET,
            'region' => 'us-east-2',
        ]);
        $this->insert('life_aws_sources' , [
            'name' => AWS::PROD_BUCKET,
            'region' => 'us-east-2',
        ]);
    }
}
