<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 13.06.2018
 * Time: 17:53
 */

namespace app\modules\patient\models\Log;


use app\modules\patient\models\Log;
use app\modules\patient\models\Patient;

class S3ErrorLog extends LogPrototype
{
    protected $aggregate = false;
    public $type = Log::TYPE_S3_SET_CONTENT;
    public $error;

    public function save() {
        $patient = Patient::findOne(['internal_id' => $this->model->internal_id]);
        if ($patient) {
            $this->model->patients_id = $patient->patients_id;
        }

        $this->model->log_content = json_encode([
            'status'    => 'failure',
            'errormsg'  => $this->error
        ]);
        return parent::save();
    }
}