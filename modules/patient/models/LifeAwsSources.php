<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 13.06.2018
 * Time: 18:17
 */

namespace app\modules\patient\models;


/**
 * This is the model class for table "life_aws_sources".
 *
 * @property string $name
 * @property string $root
 * @property string $region
 */
class LifeAwsSources extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'life_aws_sources';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'region'], 'required'],
            [['name', 'root', 'region'], 'string', 'max' => 64],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'root' => 'Root',
            'region' => 'Region',
        ];
    }
}